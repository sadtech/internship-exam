import java.util.Scanner;

public class Task1 {

    public static void main(String[] args) {
        Task1 task1 = new Task1();
        task1.runner();
    }

    public void runner() {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        String result = text.trim().replaceAll(" +", " ");
        System.out.println(result);
    }
}
