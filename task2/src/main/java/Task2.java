import java.util.Scanner;

public class Task2 {

    public static void main(String[] args) {
        Task2 task2 = new Task2();
        task2.runner();
    }

    public void runner() {
        Scanner scanner = new Scanner(System.in);

        Integer n;
        do {
            while (!scanner.hasNextInt()) {
                scanner.next();
            }
            n = scanner.nextInt();
        } while (n<1 || n>1000);

        Integer[][] arr = new Integer[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                while (!scanner.hasNextInt()) {
                    scanner.next();
                }
                arr[i][j] = scanner.nextInt();
            }
        }

        Integer sum = 0;
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (arr[i][j] == 1) {
                    sum+=1;
                }
            }
        }

        for (int i = 1; i < n; i++) {
            for (int j = 0; j <= i-1; j++) {
                if (arr[i][j] == 1 && arr[j][i]!=1) {
                    sum+=1;
                }
            }
        }
        System.out.println(sum);
    }
}
