
import java.util.HashSet;
import java.util.Objects;
import java.util.Scanner;

public class Task4 {

    private Scanner scanner = new Scanner(System.in);
    private String[][] openspace;
    private HashSet<Point> points = new HashSet<>();
    Integer size;
    Integer integer = 0;

    public static void main(String[] args) {
        Task4 task4 = new Task4();
        task4.runner();
    }

    public void runner() {
        size = getInt(3, 20);
        openspace = new String[size][size];
        String temp;
        scanner.nextLine();
        for (int i = 0; i < size; i++) {
            temp = scanner.nextLine();
            for (int j = 0; j < size; j++) {
                openspace[i][j] = String.valueOf(temp.charAt(j));
            }
        }

        Integer o = getInt(0, size);
        Integer p = getInt(0, size);

        countPoint(o, p);
        System.out.println(integer);
        points.clear();
    }


    public void countPoint(int x, int y) {
        if (openspace[x][y].equals(".")) {
            if (points.add(new Point(x, y))) {
                if (x - 1 >= 0) {
                    countPoint(x - 1, y);
                }
                if (x + 1 < size) {
                    countPoint(x + 1, y);
                }
                if (y - 1 >= 0) {
                    countPoint(x, y - 1);
                }
                if (y + 1 < size) {
                    countPoint(x, y + 1);
                }
                integer += 1;
            }
        }
    }

    public Integer getInt(Integer from, Integer to) {
        Integer temp;
        do {
            while (!scanner.hasNextInt()) {
                scanner.next();
            }
            temp = scanner.nextInt();
        } while (temp < from || temp > to);
        return temp;
    }

    private class Point {
        private int x;
        private int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Point point = (Point) o;
            return x == point.x &&
                    y == point.y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }
    }
}



