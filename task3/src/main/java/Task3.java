import java.util.Scanner;

public class Task3 {

    private Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Task3 task3 = new Task3();
        task3.runner();
    }

    public void runner() {

        Integer week, hh, mm, n;
        week = getInt(1,7);
        hh = getInt(0,23);
        mm = getInt(0,59);
        n = getInt(1,1000);

        Integer[][] arr = new Integer[n][3];
        Integer[] mas = new Integer[n];
        for (Integer[] integers : arr) {
            integers[0] = getInt(0,7);
            integers[1] = getInt(0,23);
            integers[2] = getInt(0,59);
        }

        Integer time = 0;
        if (week!=1) {
            time+=(week-1)*24*60;
        }
        time+=hh*60;
        time+=mm;

        int i = 0;
        for (Integer[] integers : arr) {
            Integer temp=0;
            if (integers[0]==0) {
                if (week!=1) {
                    temp+=(week-1)*24*60;
                }
            } else if(integers[0]!=1) {
                temp+=(integers[0]-1)*24*60;
            }
            temp+=integers[1]*60;
            temp+=integers[2];
            mas[i++] = temp;
        }
        int max = 7*60*24;
        int index = -1;
        int min_mm = max+1;
        for (int j = 0; j < n; j++) {
            int count = time;
            int minuts = 0;
            do {
                minuts+=1;
                count+=1;
                if (count==max) {
                    count=0;
                }
            } while (count!=mas[j]);
            if (min_mm>minuts) {
                min_mm=minuts;
                index = j;
            }
        }
        System.out.println(arr[index][0] + " " + arr[index][1] + " " + arr[index][2]);

    }


    public Integer getInt(Integer from, Integer to) {
        Integer temp;
        do {
            while (!scanner.hasNextInt()) {
                scanner.next();
            }
            temp = scanner.nextInt();
        } while (temp<from || temp>to);
        return temp;
    }
}
